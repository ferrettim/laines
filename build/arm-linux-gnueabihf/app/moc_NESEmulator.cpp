/****************************************************************************
** Meta object code from reading C++ file 'NESEmulator.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/include/NESEmulator.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'NESEmulator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_NESEmulator_t {
    QByteArrayData data[31];
    char stringdata0[308];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NESEmulator_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NESEmulator_t qt_meta_stringdata_NESEmulator = {
    {
QT_MOC_LITERAL(0, 0, 11), // "NESEmulator"
QT_MOC_LITERAL(1, 12, 11), // "rectChanged"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 4), // "sync"
QT_MOC_LITERAL(4, 30, 7), // "cleanup"
QT_MOC_LITERAL(5, 38, 4), // "play"
QT_MOC_LITERAL(6, 43, 5), // "pause"
QT_MOC_LITERAL(7, 49, 4), // "stop"
QT_MOC_LITERAL(8, 54, 9), // "upPressed"
QT_MOC_LITERAL(9, 64, 11), // "leftPressed"
QT_MOC_LITERAL(10, 76, 12), // "rightPressed"
QT_MOC_LITERAL(11, 89, 11), // "downPressed"
QT_MOC_LITERAL(12, 101, 12), // "startPressed"
QT_MOC_LITERAL(13, 114, 13), // "selectPressed"
QT_MOC_LITERAL(14, 128, 8), // "aPressed"
QT_MOC_LITERAL(15, 137, 8), // "bPressed"
QT_MOC_LITERAL(16, 146, 10), // "upReleased"
QT_MOC_LITERAL(17, 157, 12), // "leftReleased"
QT_MOC_LITERAL(18, 170, 13), // "rightReleased"
QT_MOC_LITERAL(19, 184, 12), // "downReleased"
QT_MOC_LITERAL(20, 197, 13), // "startReleased"
QT_MOC_LITERAL(21, 211, 14), // "selectReleased"
QT_MOC_LITERAL(22, 226, 9), // "aReleased"
QT_MOC_LITERAL(23, 236, 9), // "bReleased"
QT_MOC_LITERAL(24, 246, 7), // "loadRom"
QT_MOC_LITERAL(25, 254, 4), // "mute"
QT_MOC_LITERAL(26, 259, 19), // "handleWindowChanged"
QT_MOC_LITERAL(27, 279, 13), // "QQuickWindow*"
QT_MOC_LITERAL(28, 293, 3), // "win"
QT_MOC_LITERAL(29, 297, 4), // "rect"
QT_MOC_LITERAL(30, 302, 5) // "color"

    },
    "NESEmulator\0rectChanged\0\0sync\0cleanup\0"
    "play\0pause\0stop\0upPressed\0leftPressed\0"
    "rightPressed\0downPressed\0startPressed\0"
    "selectPressed\0aPressed\0bPressed\0"
    "upReleased\0leftReleased\0rightReleased\0"
    "downReleased\0startReleased\0selectReleased\0"
    "aReleased\0bReleased\0loadRom\0mute\0"
    "handleWindowChanged\0QQuickWindow*\0win\0"
    "rect\0color"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NESEmulator[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       2,  170, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  140,    2, 0x0a /* Public */,
       4,    0,  141,    2, 0x0a /* Public */,
       5,    0,  142,    2, 0x0a /* Public */,
       6,    0,  143,    2, 0x0a /* Public */,
       7,    0,  144,    2, 0x0a /* Public */,
       8,    0,  145,    2, 0x0a /* Public */,
       9,    0,  146,    2, 0x0a /* Public */,
      10,    0,  147,    2, 0x0a /* Public */,
      11,    0,  148,    2, 0x0a /* Public */,
      12,    0,  149,    2, 0x0a /* Public */,
      13,    0,  150,    2, 0x0a /* Public */,
      14,    0,  151,    2, 0x0a /* Public */,
      15,    0,  152,    2, 0x0a /* Public */,
      16,    0,  153,    2, 0x0a /* Public */,
      17,    0,  154,    2, 0x0a /* Public */,
      18,    0,  155,    2, 0x0a /* Public */,
      19,    0,  156,    2, 0x0a /* Public */,
      20,    0,  157,    2, 0x0a /* Public */,
      21,    0,  158,    2, 0x0a /* Public */,
      22,    0,  159,    2, 0x0a /* Public */,
      23,    0,  160,    2, 0x0a /* Public */,
      24,    1,  161,    2, 0x0a /* Public */,
      25,    1,  164,    2, 0x0a /* Public */,
      26,    1,  167,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, 0x80000000 | 27,   28,

 // properties: name, type, flags
      29, QMetaType::QRect, 0x00495103,
      30, QMetaType::QColor, 0x00095103,

 // properties: notify_signal_id
       0,
       0,

       0        // eod
};

void NESEmulator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        NESEmulator *_t = static_cast<NESEmulator *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->rectChanged(); break;
        case 1: _t->sync(); break;
        case 2: _t->cleanup(); break;
        case 3: _t->play(); break;
        case 4: _t->pause(); break;
        case 5: _t->stop(); break;
        case 6: _t->upPressed(); break;
        case 7: _t->leftPressed(); break;
        case 8: _t->rightPressed(); break;
        case 9: _t->downPressed(); break;
        case 10: _t->startPressed(); break;
        case 11: _t->selectPressed(); break;
        case 12: _t->aPressed(); break;
        case 13: _t->bPressed(); break;
        case 14: _t->upReleased(); break;
        case 15: _t->leftReleased(); break;
        case 16: _t->rightReleased(); break;
        case 17: _t->downReleased(); break;
        case 18: _t->startReleased(); break;
        case 19: _t->selectReleased(); break;
        case 20: _t->aReleased(); break;
        case 21: _t->bReleased(); break;
        case 22: { bool _r = _t->loadRom((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 23: _t->mute((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->handleWindowChanged((*reinterpret_cast< QQuickWindow*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 24:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickWindow* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (NESEmulator::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&NESEmulator::rectChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        NESEmulator *_t = static_cast<NESEmulator *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QRect*>(_v) = _t->rect(); break;
        case 1: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        NESEmulator *_t = static_cast<NESEmulator *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setRect(*reinterpret_cast< QRect*>(_v)); break;
        case 1: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject NESEmulator::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_NESEmulator.data,
      qt_meta_data_NESEmulator,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *NESEmulator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NESEmulator::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_NESEmulator.stringdata0))
        return static_cast<void*>(this);
    return QQuickItem::qt_metacast(_clname);
}

int NESEmulator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void NESEmulator::rectChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
