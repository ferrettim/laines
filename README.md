# LaiNES

LaiNES is an NES emulator for Ubuntu Touch. This project is a fork of Ryan Pattison's LaiNES port for Ubuntu Touch which is a fork of Ignacio Sanchez's LaiNES, focusing on Qt/QML and OpenGL ES for mobile phones.

<img src="laines.png" width="64">
<img src="/screenshots/landscape.png" width="256">
<img src="/screenshots/portrait.png" height="256">

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/laines.martinferretti)

## Build Instructions

### Ubuntu Touch

- [Install Clickable](http://clickable.bhdouglass.com/en/latest/)
- `clickable -c clickable.json`
- Clickable will compile the project and install it on your connected device
